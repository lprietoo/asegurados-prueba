import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
import * as $ from 'jquery'
import Swal from 'sweetalert2';
import 'bootstrap';


@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],

})

export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;
  private isEmail = /\S+@\S+\.\S+/;

  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;


  constructor(private fb: FormBuilder,) { }

  ngOnInit(): void {

    // carrousel numbers
    const totalItems = $('.main__carousel-indicators li').length;
    let currentIndex = $('div.active').index();
    $('.main__carousel-indicators span').html('/ '+totalItems)

     // disabled controls next and prev
    if(currentIndex == 0){
     $('.carousel-control-next').prop('disabled', false).addClass('active')
     $('.carousel-control-prev').prop('disabled', true).removeClass('active')
    }

    $('#carouselExampleIndicators').on('slid.bs.carousel', function () {
      currentIndex = $('div.active').index()+1;
      console.log(currentIndex)

      if(currentIndex == 1){
        $('.carousel-control-prev').prop('disabled', true).removeClass('active')
        $('.carousel-control-next').prop('disabled', false).addClass('active')
      }else{
        $('.carousel-control-prev').prop('disabled', false).addClass('active')
      }

      if(currentIndex == totalItems){
        $('.carousel-control-next').prop('disabled', true).removeClass('active')
        $('.carousel-control-prev').prop('disabled', false).addClass('active')
      }else{
        $('.carousel-control-next').prop('disabled', false).addClass('active')
      }
    });

    // Form validaton init
    this.initForm();

  }

  async onSave(): Promise<void> {
    if (this.contactForm.valid) {
      try {
        const formValue = this.contactForm.value;
        console.log(formValue)
        Swal.fire(
        'Envió exitoso',
        'Recibimos su mensaje, nos estaremos comunicando lo más pronto posible',
        'success'
        );
        this.contactForm.reset();
      } catch (e) {
        alert(e);
      }
    } else {
      Swal.fire(
        'Información incorrecta',
        'Por favor, revise que los campos estén completos o que su información sea la correcta',
        'error'
      );
    }
  }

  notRequiredHasValue(field: string): string {
    return this.contactForm.get(field).value ? 'is-valid' : '';
  }


  isValidField(field: string): string {
    const validatedField = this.contactForm.get(field);
    return (!validatedField.valid && validatedField.touched)
      ? 'is-invalid' : validatedField.touched ? 'is-valid' : '';
  }

  private initForm(): void {

    this.contactForm = this.fb.group({
      carBrand: ['', [Validators.required]],
      carModel: ['', [Validators.required]],
      year: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.isEmail)]],
      phone: ['', [Validators.required]],
    });
  }
}

