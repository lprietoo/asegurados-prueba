import { Component, OnInit } from '@angular/core';
import { faBars, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import * as $ from 'jquery'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', '../../app.component.scss',]
})
export class HeaderComponent implements OnInit {

  faBars = faBars
  faChevronDown = faChevronDown

  constructor() { }

  ngOnInit(): void {
  }

  showMenu(){
    $('.navbar-collapse').toggle();
    if($('.navbar__toggle').hasClass('collapsed')){
      $('.navbar__toggle').removeClass('collapsed');
    }else{
      $('.navbar__toggle').addClass('collapsed');
    }

  }

  showLinkFirst(){
    $('#navbarListFirst').toggle();
    if($('.menu__link--first').hasClass('show')){
      $('.menu__link--first').removeClass('show');
    }else{
      $('.menu__link--first').addClass('show');
    }

  }

  showLinkSecond(){
    $('#navbarListSecond').toggle();
    if($('.menu__link--second').hasClass('show')){
      $('.menu__link--second').removeClass('show');
    }else{
      $('.menu__link--second').addClass('show');
    }

  }

}
